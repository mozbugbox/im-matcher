#!/usr/bin/nodejs
/*
 * Test immatcher
 */

function println() {
    if (typeof console === "object") {
        console.log.apply(console, arguments);
    }
}

function main() {
    var fs, json_file, json_content, im, im_matcher, txt;

    fs = require("fs");
    json_file = "immatcher_table_py_ziranma.json";
    json_content = fs.readFileSync(json_file, "UTF-8");

    im = require("./immatcher");
    im_matcher = im.create_matcher(json_content);
    txt = "红花不是,随便的ﺫ花草鱼虫";
    println(im_matcher.find(txt, "abc"), -1);
    println(im_matcher.find(txt, "hhb"), 0);
    println(im_matcher.find(txt, ",sbd"), 4);
    println(im_matcher.find(txt, "spd"), 5);
    println(im_matcher.find(txt, "cyi"), 10);
    println(im_matcher.find(txt, "h", 3), 9);
    println(im_matcher.find("now新闻台:", "jl"), -1);
    println(im_matcher.find("一二三", "y"), 0);
    println(im_matcher.find("damn 1 03 0923", "lj"), 10);
}

main();
