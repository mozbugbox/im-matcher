#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et

"""
Search Chinese text using InputMethod initials

Copyright (C) 2016 copyright <mozbugbox@yahoo.com.au>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.


Generate input method matching table from input method data file
"""

from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import re

__version__ = "0.2"

TABLE_SEPERATOR = '|'
UNICODE_REGION = {
        "hani_comp": [0XF900, 0XFAFF],
        "hani_comp_sup": [0X2F800, 0X2FA1F],
        "hani_bmp": [0X4E00, 0X9FFF],
        "hani_exta": [0X3400, 0X4DBF],
        "hani_extb": [0X20000, 0X2A6DF],
        "hani_extc": [0X2A700, 0X2B73F],
        "hani_extd": [0X2B740, 0X2B81F],
        "hani_exte": [0X2B820, 0X2CEAF],
        }
# ZRM double py
DOUBLE_PY_SCHEME = {
        "ziranma": {
            "zh": "v",
            "ch": "i",
            "sh": "u",
            },
        "ms": {
            "zh": "v",
            "ch": "i",
            "sh": "u",
            "a": "o",
            "e": "o",
            "o": "o",
            },
        "smartabc": {
            "zh": "a",
            "ch": "e",
            "sh": "v",
            "a": "o",
            "e": "o",
            "o": "o",
            },
        }

def encode_im_hint(hint_list):
    """encode hint list into compressed string"""
    hint_text = TABLE_SEPERATOR.join(hint_list)

    # Compress a bit: x| => X
    result = re.sub(r"([a-z])\{}".format(TABLE_SEPERATOR),
            lambda g:g.group(1).upper(), hint_text)

    # compress RLE: FFFF => 4F
    result_rle = []
    last_c = []
    def pack_last_c():
        if len(last_c) <= 2:
            result_rle.append(last_c[0]*len(last_c))
        else:
            result_rle.append("{}{}".format(len(last_c), last_c[0]))

    for c in result:
        if len(last_c) == 0 or c == last_c[0]:
            last_c.append(c)
        else:
            pack_last_c()
            last_c = [c]
    if len(last_c) > 0:
        pack_last_c()
    result = "".join(result_rle)
    return result

def parse_data_file(fname, options):
    # create map for {char: "list of pinyin initials"}
    umap = {}

    print("Parsing {}...".format(fname))
    fh = io.open(fname, encoding="UTF-8")
    if options.double_pinyin:
        double_pinyin = DOUBLE_PY_SCHEME[options.double_pinyin]
    else:
        double_pinyin = None

    for line in fh:
        if line.startswith("#"): continue
        parts = line.split()
        if len(parts) < 2: continue
        # only consider pinyin of single char
        phrase = parts[1] if not options.android_data else parts[0]
        if (len(phrase) == 1):
            k = parts[0] if not options.android_data else parts[3]
            ordv = ord(phrase)
            pyk = k[0]

            if double_pinyin:
                for i in range(2, 0, -1):
                    if k[:i] in double_pinyin:
                        pyk = double_pinyin[k[:i]]
                        break

            pyk = pyk.lower()
            if ordv not in umap:
                umap[ordv] = pyk
            elif pyk not in umap[ordv]:
                umap[ordv] += pyk
    return umap

def parse_args():
    import argparse
    parser = argparse.ArgumentParser(
            description="Convert raw Input Method data to pinyin table.")
    parser.add_argument("--version", action="version", version=__version__)
    parser.add_argument("--double-pinyin", "-d",
            choices=sorted(DOUBLE_PY_SCHEME.keys()),
            help="turn on double pinyin")
    parser.add_argument("--android-data", action="store_true",
            help="data file format is android pinyin data")
    parser.add_argument("data_file", nargs=1)
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    if args.double_pinyin:
        print("** With double pinyin **")
    fname = args.data_file[0]
    umap = parse_data_file(fname, args)
    im_table = {}

    # construct pinyin table string for BMP Unihan.
    for key in sorted(UNICODE_REGION.keys()):
        result = []
        start, end = UNICODE_REGION[key]
        for i in range(start, end + 1):
            result.append(umap.get(i, ''))
        nopy = result.count('')
        total = len(result)
        print("{} Missing: {}/{}.".format(key, nopy, total))

        if nopy < total:
            result_str = encode_im_hint(result)
            im_table[key] = result_str

    base_fname = os.path.basename(fname)
    title = base_fname
    base_outname = os.path.splitext(base_fname)[0]
    if args.double_pinyin:
        title = "{} [double pinyin {}]".format(title, args.double_pinyin)
        base_outname = "{}_{}".format(base_outname, args.double_pinyin)

    output_dict = {
            "_title": "Table data from: {}".format(title),
            "filename": fname,
            "im_table": im_table,
            }

    import json
    output = "immatcher_table_{}.json".format(base_outname)
    print('Write result into "{}"'.format(output))

    val = json.dumps(output_dict, indent=2, sort_keys=True)
    with io.open(output, 'w') as fhw:
        fhw.write(f"{val}\n")

    output = "immatcher_table_{}.js".format(base_outname)
    print('Write result into "{}"'.format(output))

    with io.open(output, 'w') as fhw:
        jscode = f"const im_dict = {val};\nexport {{ im_dict }};\n"
        fhw.write(jscode)

if __name__ == '__main__':
    main()

